Run the below commands :

        1. git clone https://gitlab.com/PapalaRamesh/docker_dash_demo.git
        
        2. cd docker_dash_demo/
        
        3. sudo docker-compose build
        
        4. sudo docker-compose up
        
To close the docker, please run the below command
        
        5. sudo docker-compose down
        
